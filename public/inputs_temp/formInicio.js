const {formCreate} = require('./formCreateUser.js')
const {formLogin} = require('./formLogin.js')
const {formCreateCategory} = require('./formCreateCategory.js')
const {formUpdateCategory} = require('./formUpdateCategory.js')
const {formCreateGasto} = require('./formCreateGasto.js')
const {formUpdateGasto} = require('./formUpdateGasto.js')
const {formDeleteGasto} = require('./formDeleteGasto.js')
const {formDeleteCategory} = require('./formDeleteCategory')
const {inputList} = require('./input.js')
const { getGastosMonth, getGastosCategory, getGastosHistoryc } = require('./formConsults.js')

const mainMenu = async () => {
    let session = ""
    let option = await inputList('Seleccione una opción', [
        {value:1,name:"Iniciar Sesión"},
        {value:2,name:"Crear Cuenta"},
        {value:3,name:"Salir"}])
        console.clear()
    // when the anonumous create user login auto and create a new session
    // when some user login create a new session
    switch (option) {
        case 1:
            session = await formLogin()
            break;
        case 2:
            session = await formCreate()
            break;
        case 3:
            process.exit()
            break;
        default:
            break;
    }
    while(true){
        await inicioMenu(session)
    }
}

const inicioMenu = async (session) => {
    let option = await inputList('Seleccione una opción', [
        {value:1,name:"Categorias"},
        {value:2,name:"Gastos"},
        {value:3,name:"Consultar"},
        {value:4,name:"Cerrar Sesión"}
    ])
    console.clear()
    switch (option) {
        case 1:
            await categoriesMenu(session)
            break;
        case 2:
            await gastosMenu(session)
            break;
        case 3:
            await consultsMenu(session)
            break;
        case 4:
            await mainMenu()
            break;
        default:
            break;
    }
}

const categoriesMenu = async (session) => {
    let optionCategories = await inputList('Seleccione una opción', [
        {value:1,name:"Crear Categoria"},
        {value:2,name:"Actualizar Categoria"},
        {value:3,name:"Eliminar Categoria"},
        {value:4,name:"Cancelar"}
    ])
    console.clear()
    switch (optionCategories) {
        case 1:
            await formCreateCategory(session)
            break;
        case 2:
            await formUpdateCategory(session)
            break;
        case 3:
            await formDeleteCategory(session)
            break;
        case 4:
            break;
        default:
            break;
    }
}

const gastosMenu = async (session) => {
    let optionGastos = await inputList('Seleccione una opción', [
        {value:1,name:"Registrar Gasto"},
        {value:2,name:"Actualizar Gasto"},
        {value:3,name:"Eliminar Gasto"},
        {value:4,name:"Cancelar"}
    ])
    console.clear()
    switch (optionGastos) {
        case 1:
            await formCreateGasto(session)
            break;
        case 2:
            await formUpdateGasto(session)
            break;
        case 3:
            await formDeleteGasto(session)
            break;
        case 4:
            break;
        default:
            break;
    }
}

const consultsMenu = async (session) => {
    let optionConsults = await inputList('Seleccione una opción', [
        {value:1,name:"Gastos Mes"},
        {value:2,name:"Gastos Categorias"},
        {value:3,name:"Historico de Gastos"},
        {value:4,name:"Cancelar"}
    ])
    console.clear()
    switch (optionConsults) {
        case 1:
            await getGastosMonth(session)
            break;
        case 2:
            await getGastosCategory(session)
            break;
        case 3:
            await getGastosHistoryc(session)
            break;
        case 4:
            break;
        default:
            break;
    }
}

module.exports = {
    mainMenu
}