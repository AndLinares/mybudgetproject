const {inputText, inputDate, inputList, inputNumber} = require('./input.js')
const {getCategories, newGasto} = require('../../src/controlers/inputGastos.js')

const formCreateGasto = async (session) => {
    // This will be a form in HTML and CSS
    let date = await inputDate('Cual es la fecha del gasto? ')
    let description = await inputText('Descripción del gasto: ')
    let Monto = await inputNumber('De cuanto fue el gasto?')
    let category = await inputList('Cual categoria', await getCategories(session) )
    /* This will be part of verify on Front maybe in the controller and some parts with react or angular
        This part verify if all information be in a correct format before process.
    */
    if (date == '' || description == '' || Monto == '' || category == '') {
        console.log('Algun campo esta vacio por favor vuelva a intentar');
    } else if(Monto > 0) {
        let gastoData = {
            user: session,
            date: date,
            description: description,
            monto: Monto,
            category: category
        }
        newGasto(gastoData)
    }else{
        console.log('No se puede registrar montos negativos o cero');
    }
    
}

module.exports = {formCreateGasto}