const {inputText, inputDate, inputList, inputNumber} = require('./input.js')
const {getCategories, setGasto, getGastosList} = require('../../src/controlers/inputGastos.js')

const formUpdateGasto = async (session) => {
    // This will be a form in HTML and CSS
    // which gasto?
    let gasto = await inputList('Cual gasto ?', await getGastosList(session))
    let oldGasto = Object.assign({}, gasto)
    let option = await inputList('Cual campo ? ', [
        {value:1, name:'Fecha del gasto'},
        {value:2, name:'descripción'},
        {value:3, name:'Monto'},
        {value:4, name:'categoria'}
    ])
    // new data
    switch (option) {
        case 1:
            gasto.date = await inputDate('Cual es la fecha del gasto? ')
            break;
        case 2:
            gasto.description = await inputText('Descripción del gasto: ')
            break;
        case 3:
            gasto.monto = await inputNumber('De cuanto fue el gasto? (solo numeros sin comas ni letras)')
            break;
        case 4:
            gasto.category = await inputList('Cual categoria', await getCategories(session) )
            break;
        default:
            console.log('No se escogio nada');
            break;
    }
    /* This will be part of verify on Front maybe in the controller and some parts with react or angular
        This part verify if all information be in a correct format before process.
    */
    if (gasto.date == '' || gasto.description == '' || gasto.monto == '' || gasto.category == '') {
        console.log('Algun campo esta vacio por favor vuelva a intentar');
    } else if(gasto.monto > 0) {
        setGasto({gasto: oldGasto, newGasto: gasto})
    }else{
        console.log('No se puede registrar montos negativos o cero');
    }
}

module.exports = {formUpdateGasto}