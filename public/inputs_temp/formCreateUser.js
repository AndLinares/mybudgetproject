const {inputText, inputPassword} = require('./input.js')
const {getCreateUser} = require('../../src/controlers/inputUser.js')

const formCreate = async () => {
    // This will be a form in HTML and CSS
    let name = await inputText('Cual es tu nombre? ')
    let lastName = await inputText('Cual es tu apellido? ')
    let email = await inputText('Cual es tu correo Electronico? ')
    let password = await inputPassword('Define una contraseña ')
    let repeatPassword = await inputPassword('Repite tu contraseña')
    /* This will be part of verify on Front maybe in the controller and some parts with react or angular
        This part verify if all information be in a correct format before process.
    */
    if (name == '' || lastName == '' || email == '' || password == '') {
        console.log('Algun campo esta vacio por favor vuelva a intentar');
    } else if (password != repeatPassword) {
        console.log('Las contraseñas no coinciden')
    } else if (!email.includes('@')){
        console.log('Correo electronico no valido por falta de un @')
    } else if (!email.includes('.')){
        console.log('Correo electronico no valido por falta de un .')
    } else {
        let userData = {
            name: name,
            lastName: lastName,
            email: email,
            password: password
        }
        await getCreateUser(userData)
        return userData.email
    }
}

module.exports = {formCreate}