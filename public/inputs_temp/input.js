// Constantes de las librerias
const inquirer = require('inquirer')
inquirer.registerPrompt('datepicker', require('inquirer-datepicker-prompt'));

/**
 * 
 * @param {String} query is the Question (or text) that you want show at the user. 
 * @returns the text that user input
 */
const inputText = async (query) => {
    let options = [{
        name:"inputText",
        type:"input",
        message: query
    }]
    let value = await inquirer.prompt(options)
    return value.inputText
}

/**
 * 
 * @param {String} query is the Question (or text) that you want show at the user. 
 * @returns the text that user input
 */
 const inputPassword = async (query) => {
    let options = [{
        name:"inputText",
        type:"password",
        message: query
    }]
    let value = await inquirer.prompt(options)
    return value.inputText
}

/**
 * 
 * @param {String} query is the Question (or text) that you want show at the user. 
 * @param {Array} list The array of options must contains JSONs' whit value and name like Keys 
 * @returns this return the value of the options
 */
const inputList = async (query, list) => {
    const options = [{
            name:"inputList",
            type: "list",
            message: query,
            choices: list
    }]
    let value = await inquirer.prompt(options)
    return value.inputList
}

/**
 * 
 * @param {String} query is the Question (or text) that you want show at the user. 
 * @returns the text that user input, if the user input a string, this function print a error and repeat the Question
 */
const inputNumber = async (query) => {
    while(true){
        let options = [{
            name:"inputNumber",
            type:"number",
            message: query
        }]
        let value = await inquirer.prompt(options)
        if(!isNaN(value.inputNumber)){
            return value.inputNumber
        }else{
            console.log("Ingresé solo números, por favor");
        }
    } 
}

const inputDate = async (query) => {
    let options = [{
        name:"date",
        type:"datepicker",
        message: query,
        format: ['m', '/', 'd', '/', 'yy', ' ', 'h', ':', 'MM', ' ', 'TT']
    }]
    let value = await inquirer.prompt(options)
    return value.date.toJSON()
}

module.exports = {
    inputText,
    inputPassword,
    inputList,
    inputNumber,
    inputDate
}