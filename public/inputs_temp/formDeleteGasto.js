const {inputList} = require('./input.js')
const {setDeleteGasto, getGastosList} = require('../../src/controlers/inputGastos.js')

const formDeleteGasto = async (session) => {
    // This will be a form in HTML and CSS
    // which gasto?
    let gasto = await inputList('Cual gasto quiere eliminar? ', await getGastosList(session))
    let option = await inputList('Estas seguro ? ', [
        {value:1, name:'Aceptar'},
        {value:2, name:'Cancelar'}
    ])
    /* This will be part of verify on Front maybe in the controller and some parts with react or angular
        This part verify if all information be in a correct format before process.
    */
    if(option == 1){
        setDeleteGasto(gasto)
    }
}

module.exports = {formDeleteGasto}