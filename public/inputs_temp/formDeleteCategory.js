const {inputList} = require('./input.js')
const {setDeleteCategory, getCategories, deleteGastos, saveGastos} = require('../../src/controlers/inputCategory.js')

const formDeleteCategory = async (session) => {
    // This will be a form in HTML and CSS
    let name = await inputList('Cual categoria', await getCategories(session) )
    let deleteAll = await inputList('Eliminar Gastos ? ', [
        {value:1, name:'Conservar Gastos'},
        {value:2, name:'Eliminar Gastos'}
    ])
    let option = await inputList('Estas seguro de eliminar la categoria ? ', [
        {value:1, name:'Aceptar'},
        {value:2, name:'Cancelar'}
    ])
    // for can send information
    let data = {
        user: session,
        category: name
    }
    if(option == 1){
        let delte = setDeleteCategory(data) != false
        if(deleteAll == 2 && delte != false){
            deleteGastos(data)
        }else if(delte != false){
            saveGastos(data)
        }
    }
    
}

module.exports = {
    formDeleteCategory
}