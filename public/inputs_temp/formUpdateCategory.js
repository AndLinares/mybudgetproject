const {inputText, inputList} = require('./input.js')
const {setCategory, getCategories} = require('../../src/controlers/inputCategory.js')

const formUpdateCategory = async (session) => {
    // This will be a form in HTML and CSS
    let name = await inputList('Cual categoria', await getCategories(session) )
    let newName = await inputText('Cual es el nuevo nombre de la categoria? ')
    // for can send information
    let data = {
        user: session,
        category: name,
        newCategory: newName
    }
    setCategory(data)
}

module.exports = {
    formUpdateCategory
}