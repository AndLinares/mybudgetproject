const {inputList} = require('./input.js')
const {getGastoMonth, getCategories, getGastoCategory, getGastos, getGastosAllCategories, getGastosAllMonth} = require('../../src/controlers/inputGastos.js')

const getGastosMonth = async (session) => {
    let month = await inputList('Escoja el mes a consultar', [
        {value:12, name:"Todos los meses"},
        {value:0, name:"Enero"},
        {value:1, name:"Febrero"},
        {value:2, name:"Marzo"},
        {value:3, name:"Abril"},
        {value:4, name:"Mayo"},
        {value:5, name:"Junio"},
        {value:6, name:"Julio"},
        {value:7, name:"Agosto"},
        {value:8, name:"Septiembre"},
        {value:9, name:"Octubre"},
        {value:10, name:"Noviembre"},
        {value:11, name:"Diciembre"}
    ])
    if(month == 12){
        let gastos = await getGastosAllMonth(session)
        for (let index = 0; index < 12; index++) {
            await template(gastos[index])
        }
    }else{
        let gastos = await getGastoMonth(session, month)
        await template(gastos)
    }
    
    console.log('--------------------------------------------------------------------');
} 

const getGastosCategory = async (session) => {
    let listCategories = await getCategories(session)
    listCategories.unshift({value:'All', name:"Todas las Categorias"})
    let Category = await inputList('Escoja la categoria a consultar', listCategories)
    if(Category == 'All'){
        listCategories.shift()
        let list = []
        for (let index = 0; index < listCategories.length; index++) {
            list.push(listCategories[index].value)
        }
        let gastos = await getGastosAllCategories(session, list)
        for (let index = 0; index < gastos.length; index++) {
            await template(gastos[index])
        }
    }else{
        let gastos = await getGastoCategory(session, Category)
        await template(gastos)
    }
    
    console.log('--------------------------------------------------------------------');
}

const getGastosHistoryc = async (session) => {
    let gastos = await getGastos(session)
    await template(gastos)
    console.log('--------------------------------------------------------------------');
}

const template = async (gastos) => {
    if (gastos.length == 0){
    }else{
        gastos.forEach(async(element) => {
            let month = await isMonth(element.date.getMonth())
            console.log(
`--------------------------------------------------------------------
Mes: ${month}
Fecha: ${element.date}       
categoria: ${element.category}
Monto: ${element.monto}
Descripción: ${element.description}`);}
    );
    }
}


const isMonth = async (month) => {
    switch (month) {
        case 0:
            return 'Enero'
        case 1:
            return 'Febrero'
        case 2:
            return 'Marzo'
        case 3:
            return 'Abril'
        case 4:
            return 'Mayo'
        case 5:
            return 'Junio'
        case 6:
            return 'Julio'
        case 7:
            return 'Agosto'
        case 8:
            return 'Septiembre'
        case 9:
            return 'Octubre'
        case 10:
            return 'Noviembre'
        case 11:
            return 'Diciembre'
        default:
            break;
    }
}

module.exports = {
    getGastosMonth,
    getGastosCategory,
    getGastosHistoryc
}