const {inputText} = require('./input.js')
const {setCreateCategory} = require('../../src/controlers/inputCategory.js')

const formCreateCategory = async (session) => {
    // This will be a form in HTML and CSS
    let name = await inputText('Cual es el nombre de la categoria? ')
    // for can send information
    let data = {
        user: session,
        newCategory: name
    }
    setCreateCategory(data)
}

module.exports = {
    formCreateCategory
}
