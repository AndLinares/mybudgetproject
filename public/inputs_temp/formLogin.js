const {inputText, inputPassword} = require('./input.js')
const {compareLogin} = require('../../src/controlers/inputUser')

const formLogin = async () => {
    // This will be a form in HTML and CSS
    let email = await inputText('Digite el email del usuario ')
    let password = await inputPassword('Digite la contraseña')

    /* This will be part of verify on Front maybe in the controller and some parts with react or angular
        This part verify if all information be in a correct format before process.
    */
        if (email == ''|| password == '') {
            console.log('Algun campo esta vacio por favor vuelva a intentar');
        } else if (!email.includes('@')){
            console.log('Correo electronico no valido por fata de un @')
        } else if (!email.includes('.')){
            console.log('Correo electronico no valido por falta de un .')
        } else {
            let userData = {
                email: email,
                password: password
            }
            console.log('Cargando ...');
            await compareLogin(userData)
            return userData.email
        }
}

module.exports = {
    formLogin
}