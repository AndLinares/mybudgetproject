const fs = require('fs');
const path = require('path');

let rutaArchivo = path.join(__dirname, 'data_temp/users.json');
let rutaArchivoCategories = path.join(__dirname, 'data_temp/categories.json')
let rutaArchivoGastos = path.join(__dirname, 'data_temp/gastos.json')

const createUser = async function (data) {
    try{
        console.log('Creando el usuario');
        if (fs.existsSync(rutaArchivo)){
            let archivo = JSON.parse(fs.readFileSync(rutaArchivo));
            // this add new user in the array
            archivo.user.push(data);
            let nuevoArchivo = JSON.stringify(archivo);
            // this change the JSON
            fs.writeFileSync(rutaArchivo ,nuevoArchivo, "utf-8");
            console.log('Se creo el usuario exitosamente');
        }        
        return true
    }catch(err){
        console.log('No se pudo crear el Usuario intentelo de nuevo');
        console.log(err);
        return false
    }
}

const consultUser = async (email) => {
    try {
        if (fs.existsSync(rutaArchivo)){
            let archivo = JSON.parse(fs.readFileSync(rutaArchivo));
            let elemento = archivo.user.filter((elemento)=>{
                // find the user with the email and return all data about one user
                return elemento.email == email
            })
            // This verefy if the user exist
            if (elemento.length == 0){
                return false
            }
            return elemento
        }
    } catch (err) {
        console.log('No se encontro al usuario intentelo de nuevo');
        console.log(err);
        return false
    }
}

const initCategories = async (data) => {
    try{
        console.log('inicializando usuario');
        if (fs.existsSync(rutaArchivoCategories)){
            let archivo = JSON.parse(fs.readFileSync(rutaArchivoCategories));
            // this add new Category in the array
            archivo.Categories.push(data)
            let nuevoArchivo = JSON.stringify(archivo);
            // this change the JSON
            fs.writeFileSync(rutaArchivoCategories ,nuevoArchivo, "utf-8");
            console.log('Se creo la categoria exitosamente');
        }        
        return true
    }catch(err){
        console.log('No se pudo crear la categoria intentelo de nuevo');
        console.log(err);
        return false
    }
}

const createCategory = async function (data) {
    try{
        console.log('Creando la categoria');
        if (fs.existsSync(rutaArchivoCategories)){
            let archivo = JSON.parse(fs.readFileSync(rutaArchivoCategories));
            // this add new Category in the array
            archivo.Categories.filter((elemento)=>{
                if(elemento.user == data.user){
                    elemento.categories.push(data.newCategory)
                }
                return true
            });
            let nuevoArchivo = JSON.stringify(archivo);
            // this change the JSON
            fs.writeFileSync(rutaArchivoCategories ,nuevoArchivo, "utf-8");
            console.log('Se creo la categoria exitosamente');
        }        
        return true
    }catch(err){
        console.log('No se pudo crear la categoria intentelo de nuevo');
        console.log(err);
        return false
    }
}

const consultCategory = async (session) => {
    try {
        if (fs.existsSync(rutaArchivoCategories)){
            let archivo = JSON.parse(fs.readFileSync(rutaArchivoCategories));
            let elemento = archivo.Categories.filter((elemento)=>{
                // find the user with the session and return all categories
                return elemento.user == session
            })
            // This verefy if the user have categories
            if (elemento.length == 0){
                return false
            }
            return elemento
        }
    } catch (err) {
        console.log('No se encontro la categoria');
        console.log(err);
        return false
    }
}

const updateCategory = async (data) => {
    try {
        if (fs.existsSync(rutaArchivoCategories)){
            let archivo = JSON.parse(fs.readFileSync(rutaArchivoCategories));
            let elemento = archivo.Categories.filter((elemento)=>{
                // find the user with the session and return all categories
                return elemento.user == data.user
            })
            // This verefy if the user have categories
            if (elemento.length == 0){
                return false
            }else{
                let index = elemento[0].categories.findIndex((element)=>{
                    return element == data.category
                })
                elemento[0].categories[index] = data.newCategory
                let nuevoArchivo = JSON.stringify(archivo);
                fs.writeFileSync(rutaArchivoCategories ,nuevoArchivo, "utf-8");
                console.log('Se actualizo la categoria exitosamente');
            }
            return elemento
        }
    } catch (err) {
        console.log('No se encontro la categoria');
        console.log(err);
        return false
    }
}

const deleteCategory = async (data) => {
    try {
        if (fs.existsSync(rutaArchivoCategories)){
            let archivo = JSON.parse(fs.readFileSync(rutaArchivoCategories));
            let elemento = archivo.Categories.filter((elemento)=>{
                // find the user with the session and return all categories
                return elemento.user == data.user
            })
            // This verefy if the user have categories
            if (elemento.length == 0){
                return false
            }else{
                let index = elemento[0].categories.findIndex((element)=>{
                    return element == data.category
                })
                elemento[0].categories.splice(index, 1)
                let nuevoArchivo = JSON.stringify(archivo);
                fs.writeFileSync(rutaArchivoCategories ,nuevoArchivo, "utf-8");
                console.log('Se elimino la categoria exitosamente');
            }
            return elemento
        }
    } catch (err) {
        console.log('No se encontro la categoria');
        console.log(err);
        return false
    }
}

const createGasto = async function (data) {
    try{
        console.log('Creando nuevo Gasto');
        if (fs.existsSync(rutaArchivoGastos)){
            let archivo = JSON.parse(fs.readFileSync(rutaArchivoGastos));
            // this add new Category in the array
            archivo.Gastos.push(data)
            let nuevoArchivo = JSON.stringify(archivo);
            // this change the JSON
            fs.writeFileSync(rutaArchivoGastos ,nuevoArchivo, "utf-8");
            console.log('Se creo el gasto exitosamente');
        }        
        return true
    }catch(err){
        console.log('No se pudo crear el gasto intentelo de nuevo');
        console.log(err);
        return false
    }
}

const consultGastos = async (session) => {
    try {
        if (fs.existsSync(rutaArchivoGastos)){
            let archivo = JSON.parse(fs.readFileSync(rutaArchivoGastos));
            let elemento = archivo.Gastos.filter((elemento)=>{
                // find the user with the session and return all categories
                return elemento.user == session
            })
            // This verefy if the user have categories
            if (elemento.length == 0){
                return false
            }
            return elemento
        }
    } catch (err) {
        console.log('No se encontro la categoria');
        console.log(err);
        return false
    }
}

const consultGastosByCategory = async (data) => {
    try {
        if (fs.existsSync(rutaArchivoGastos)){
            let archivo = JSON.parse(fs.readFileSync(rutaArchivoGastos));
            let elemento = archivo.Gastos.filter((elemento)=>{
                // find the user with the session and return all categories
                return elemento.user == data.user && elemento.category == data.category
            })
            // This verefy if the user have categories
            if (elemento.length == 0){
                return false
            }
            return elemento
        }
    } catch (err) {
        console.log('No se encontro la categoria');
        console.log(err);
        return false
    }
}

const UpdateGasto = async (data) => {
    try {
        if (fs.existsSync(rutaArchivoGastos)){
            let archivo = JSON.parse(fs.readFileSync(rutaArchivoGastos));
            let index = archivo.Gastos.findIndex((elemento)=>{
                // find the user with the session and return all categories
                return JSON.stringify(elemento) == JSON.stringify(data.gasto)
            })
            // This verefy if the user have categories
            if (index == -1){
                return false
            }else{
                archivo.Gastos[index] = data.newGasto
                let nuevoArchivo = JSON.stringify(archivo);
                fs.writeFileSync(rutaArchivoGastos ,nuevoArchivo, "utf-8");
                console.log('Se actualizo el gasto');
            }
        }
    } catch (err) {
        console.log('No se encontro la categoria');
        console.log(err);
        return false
    }
}

const deleteGasto = async (data) => {
    try {
        if (fs.existsSync(rutaArchivoGastos)){
            let archivo = JSON.parse(fs.readFileSync(rutaArchivoGastos));
            let index = archivo.Gastos.findIndex((elemento)=>{
                // find the user with the session and return all categories
                return JSON.stringify(elemento) == JSON.stringify(data)
            })
            // This verefy if the user have categories
            if (index == -1){
                return false
            }else{
                archivo.Gastos.splice(index, 1)
                let nuevoArchivo = JSON.stringify(archivo);
                fs.writeFileSync(rutaArchivoGastos ,nuevoArchivo, "utf-8");
                console.log('Se elimino el gasto');
            }
        }
    } catch (err) {
        console.log('No se encontro la categoria');
        console.log(err);
        return false
    }
}

module.exports = {
    createUser,
    consultUser, 
    createCategory, 
    consultCategory, 
    initCategories, 
    updateCategory,
    createGasto,
    consultGastos,
    consultGastosByCategory,
    UpdateGasto,
    deleteGasto,
    deleteCategory
}