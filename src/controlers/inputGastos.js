const {consultCategory, createGasto, consultGastos, UpdateGasto, deleteGasto} = require('../models/consultDB.js')

const getCategories = async (session) => {
    let list = []
    let user = await consultCategory(session)
    user[0].categories.forEach(element => {
        list.push({value:element, name:element})
    });
    return list
}

const newGasto = async (data) => {
    createGasto(data)
}

const getGastosList = async (session) => {
    let list = []
    let user = await consultGastos(session)
    user.forEach(element => {
        list.push({value:element, name:`${new Date(element.date)}
        Monto: ${element.monto} categoria: ${element.category} 
        Descripción: ${element.description}`})
    });
    return list
}

const setGasto = async (data) => {
    console.log('Actualizando Gasto');
    UpdateGasto(data); 
}

const setDeleteGasto = async (data) => {
    console.log('Eliminando Gasto');
    deleteGasto(data)
}

const getGastoMonth = async (session, month) => {
    let gastos = await consultGastos(session)
    if(gastos){
        gastos.sort(function(a, b) {
            if (a.date > b.date) return -1;
            else if (a.date < b.date) return 1;
            else return 0;
        })
        gastos.forEach(element => {
            element.date = new Date(element.date)
        });
        let monthGastos = gastos.filter(element => {
            return element.date.getMonth() == month
        })
        return monthGastos
    }
}

const getGastoCategory = async (session, category) => {
    let gastos = await consultGastos(session)
    gastos.sort(function(a, b) {
        if (a.date > b.date) return -1;
        else if (a.date < b.date) return 1;
        else return 0;
    })
    gastos.forEach(element => {
        element.date = new Date(element.date)
    });
    let categoryGastos = gastos.filter(element => {
        return element.category == category
    })
    return categoryGastos
}

const getGastos = async (session) => {
    let gastos = await consultGastos(session)
    gastos.sort(function(a, b) {
        if (a.date > b.date) return -1;
        else if (a.date < b.date) return 1;
        else return 0;
    })
    gastos.forEach(element => {
        element.date = new Date(element.date)
    });
    return gastos
}

const getGastosAllMonth = async (session) => {
    let gastos = []
    for (let index = 0; index < 12; index++) {
        gastos.push(await getGastoMonth(session, index))
    }
    return gastos
}

const getGastosAllCategories = async (session, categories) => {
    let gastos = []
    for (let index = 0; index < categories.length; index++) {
       gastos.push(await getGastoCategory(session, categories[index]))
    }
    return gastos
}

module.exports = {
    getCategories,
    newGasto,
    getGastosList,
    setGasto,
    setDeleteGasto,
    getGastoMonth,
    getGastoCategory,
    getGastos,
    getGastosAllCategories,
    getGastosAllMonth
}