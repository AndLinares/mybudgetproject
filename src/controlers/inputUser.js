const bcrypt = require("bcryptjs");
const {createUser, consultUser, initCategories} = require('../models/consultDB.js')

// this script is for all thing about the users and their sessions 

const getCreateUser = async (user) => {
    // verify if the user input a correct data, if not print a error
    if(user){
        // this verify if exist some user with same email
        let isExist = await consultUser(user.email);
        if(!isExist){
            // This encrypt the password with 11 jumps of security
            let Hash = await bcrypt.hash(user.password, 11);
            // This is the true format for send to DB
            let userDB = {
                name: `${user.name} ${user.lastName}`,
                email: user.email,
                passwordHash: Hash
            }
            if (createUser(userDB)) {
                initCategories({user:user.email, categories:["Sin Categoria"]})
            }
            return true
        }else{
            console.log('El usuario ya existe');
        }
        
    }else{
        console.log('No se pudo crear el usuario intentelo de nuevo');
    }
}

const compareLogin = async (user) => {
    // get user data
    let userData = await consultUser(user.email)
    // this verifi if the user exist
    if(userData){
        // this compare the password
        let isEquals = await bcrypt.compare(user.password, userData[0].passwordHash)
        if(isEquals){
            console.log('Ingreso a la aplicación');
            return true
        }else{
            console.log('contraseña incorrecta');
            return false
        }
    }else{
        console.log('Usuario no valido');
    }   
}

module.exports = {getCreateUser, compareLogin}

