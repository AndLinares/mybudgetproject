const {
    createCategory, 
    consultCategory, 
    updateCategory, 
    deleteCategory, 
    consultGastosByCategory,
    deleteGasto,
    UpdateGasto
    } = require('../models/consultDB.js')

const getCategories = async (session) => {
    let list = []
    let user = await consultCategory(session)
    user[0].categories.forEach(element => {
        list.push({value:element, name:element})
    });
    return list
}

const setCreateCategory = async (data) => {
    if(data){
        let user = await consultCategory(data.user)
        if(user){
            let elemento = user[0].categories.filter((elemento) => {
                return elemento == data.newCategory
            })
            if(elemento.length == 0){
                createCategory(data)
            }else{
                console.log('Ya existe esa categoria');
            }
        }else{
            console.log('Hubo un error en la aplicación intentelo más tarde');
        }
    }
}

const setCategory = async (data) => {
    if(data.category != 'Sin Categoria'){
        let user = await consultCategory(data.user)
        if(user){
            // verefy that the new name not exist
            let elemento = user[0].categories.filter((elemento) => {
                return elemento == data.newCategory
            })
            if(elemento.length == 0){
                // verefy if the updated category exist
                let elemento2 = user[0].categories.filter((elemento) => {
                    return elemento == data.category
                })
                if (elemento2.length != 0){
                    updateCategory(data)
                }
                    else{
                        console.log('No existe la categoria a cambiar');
                }
            }else{
                console.log('Ya existe esa categoria');
            }
        }else{
            console.log('Hubo un error en la aplicación intentelo más tarde');
        }
    }else{
        console.log('No se puede cambiar el nombre de la categoria "Sin Categoria"');
    }
}

const setDeleteCategory = async (data) => {
    if(data.category != 'Sin Categoria'){
        let user = await consultCategory(data.user)
        if(user){
            // verefy if the updated category exist
            let elemento2 = user[0].categories.filter((elemento) => {
                return elemento == data.category
            })
            if (elemento2.length != 0){
                deleteCategory(data)
            }
                else{
                    console.log('No existe la categoria a eliminar');
            }
        }else{
            console.log('Hubo un error en la aplicación intentelo más tarde');
        }
    }else{
        console.log('No se puede eliminar la categoria "Sin Categoria"');
        return false
    }
}

const deleteGastos = async (data) => {
    let gastos = await consultGastosByCategory(data)
    if(gastos.length){
        gastos.forEach(element => deleteGasto(element))
    }
}

const saveGastos = async (data) => {
    let gastos = await consultGastosByCategory(data)
    if(gastos.length){
        gastos.forEach(element => deleteGasto(element))
        gastos.forEach(element =>  {
        let oldGasto = Object.assign({}, element)
        element.category = "Sin Categoria"
        UpdateGasto({
            gasto: oldGasto,
            newGasto: element
            })
        })
    }
}

module.exports = {
    getCategories,
    setCreateCategory,
    setCategory,
    setDeleteCategory,
    deleteGastos,
    saveGastos
}